package com.example.hso.controller;


import com.example.hso.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/testBoot")
public class HelloController {

    @Autowired
    private UserServiceImpl userService;
    @RequestMapping("getUser/{id}")
    public String GetUser(@PathVariable int id) {
        return userService.get(id).toString();
    }

}
