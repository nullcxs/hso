package com.example.hso.service;

import com.example.hso.entity.User;
import com.example.hso.mapper.UserMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl {
    @Resource
  UserMapper userMapper;

    public User get(int id) {
        // 通过Mapper的select方法查询用户
        return userMapper.Sel(id);
    }

}
