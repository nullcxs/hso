package com.example.hso;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.example.hso.mapper")
@SpringBootApplication
public class HsoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HsoApplication.class, args);
    }

}
